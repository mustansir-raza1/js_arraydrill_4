// Define the each function
function each(elements, cb) {
    if(Array.isArray(elements)){
        for (var index = 0; index < elements.length; index++) {
            cb(elements[index], index);
        }
    }
    else{
        return []
    }
}
// Export the each function
module.exports = each;
