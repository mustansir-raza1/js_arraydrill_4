// Do NOT use .includes, to complete this function.
    // Look through each value in `elements` and pass each element to `cb`.
    // If `cb` returns `true` then return that element.
    // Return `undefined` if no elements pass the truth test.

// create a function find find with two arguments element and callback function

function find(element, cb){
    if(Array.isArray(element)){
        for(let index = 0; index < element.length; index++){
            if((element[index]==cb)){
                return index;
            }
        }
        return undefined;
    }
    else{
        return []
    }
}
module.exports = find;
