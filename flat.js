// Flattens a nested array (the nesting can be to any depth).
    // Hint: You can solve this using recursion.
    // Example: flatten([1, [2], [3, [[4]]]]); => [1, 2, 3, 4];

function flatten(elements) {
    if(Array.isArray(elements)){
        let flattenedArray = [];
        elements.forEach(element => {
            if (Array.isArray(element)) {
                flattenedArray = flattenedArray.concat(flatten(element));
            } else {
                flattenedArray.push(element);
            }
        });
        return flattenedArray;
    }
    else{
        return []
    }
   
}
module.exports = flatten;