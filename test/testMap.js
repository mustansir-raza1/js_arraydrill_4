const myMap = require('../map');

try{
    const numbers = [1, 2, 3, 4, 5, 5];
    const squaredNumbers = myMap(numbers, (num) => num * num);
    console.log(squaredNumbers); // Output: [1, 4, 9, 16, 25]
}
catch(error){
    console.log("error")
}