function map(elements, cb) {
    if (Array.isArray(elements)){
        const mappedArray = [];
        for (let i = 0; i < elements.length; i++) {
            mappedArray.push(cb(elements[i]));
        }
        return mappedArray;
    }
    else{
        return []
    }
    
}

module.exports = map;
