function filter(elements) {
    // Do NOT use .filter, to complete this function.
    // Similar to `find` but you will return an array of all elements that passed the truth test
    // Return an empty array if no elements pass the truth test

    if(Array.isArray(elements)){
        
        let newArray = []
        for (let index = 0; index < elements.length; index++){
            if((elements[index] % 2 == 1)){
                newArray.push(elements[index])
            }
        }
        return newArray;
    }
    else{
        return []
    }
    
}
module.exports = filter;
